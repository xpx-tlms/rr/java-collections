package com.xpanxion.java.collections;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class Worker {

    //
    // Data Members
    //

    // Lists
    private final List<Person> stronglyTypedList;
    private final ArrayList weaklyTypedList = new ArrayList();  // Not preferred, kept in Java for backwards compatiblity.

    // Queues
    private final Queue<Person> personQueue = new LinkedList<>();
    private final Queue<Person> personPriorityQueue = new PriorityQueue<>();

    // Stacks
    private final Stack<Person> personStack = new Stack<>();

    // Maps
    private final HashMap<String, Person> personMap = new HashMap<>();
    private final Dictionary<String, Person> personDictionary = new Hashtable<String, Person>(); // Obsolete, do not use.

    // Sets
    private final HashSet<Person> personSet1 = new HashSet<>();
    private final HashSet<Person> personSet2 = new HashSet<>();

    //
    // Constructors
    //

    public Worker() {
        // Create weakly typed list (not recommended).
        this.weaklyTypedList.add(new Person(12, "John", "Smith"));
        this.weaklyTypedList.add(new Person(23, "Mary", "Jane"));
        this.weaklyTypedList.add(new Person(67, "Rick", "Jone"));
        this.weaklyTypedList.add(new Person(56, "Patty", "Same"));
        this.weaklyTypedList.add(new Person(47, "Sue", "Anderson"));
        //this.weeklyTypedList.add(new Cat(4,"Gypsy", "Black")); // This crashes the app.

        // Create strongly typed list (recommended).
        this.stronglyTypedList = Arrays.asList(
            new Person(12, "John", "Smith"),
            new Person(23, "Mary", "Jane"),
            new Person(67, "Rick", "Jone"),
            new Person(56, "Patty", "Same"),
            new Person(47, "Sue", "Anderson")
        );

        // Queues.
        this.personQueue.add(new Person(12, "John", "Smith"));
        this.personQueue.add(new Person(23, "Mary", "Jane"));
        this.personQueue.add(new Person(67, "Rick", "Jone"));

        this.personPriorityQueue.add(new Person(35, "John", "Smith"));
        this.personPriorityQueue.add(new Person(47, "Paul", "Jones"));
        this.personPriorityQueue.add(new Person(12, "Tom", "Moda"));
        this.personPriorityQueue.add(new Person(66, "Mary", "Williams"));

        // Stacks.
        this.personStack.push(new Person(12, "John", "Smith"));
        this.personStack.push(new Person(42, "Paul", "Jones"));
        this.personStack.push(new Person(52, "Tom", "Moda"));

        // Maps.
        this.personMap.put("111-11-1111", new Person(12, "John", "Smith"));
        this.personMap.put("222-22-2222", new Person(42, "Paul", "Jones"));
        this.personMap.put("333-33-3333", new Person(52, "Tom", "Moda"));

        this.personDictionary.put("111-11-1111",new Person(12, "John", "Smith"));
        this.personDictionary.put("222-22-2222", new Person(42, "Paul", "Jones"));
        this.personDictionary.put("333-33-3333", new Person(52, "Tom", "Moda"));

        // HashSets.
        this.personSet1.add(new Person(12, "John", "Snow")); // Also in set 2.
        this.personSet1.add(new Person(12, "John", "Snow")); // This is a duplicate and does not get added.
        this.personSet1.add(new Person(24, "Mary", "Williams"));
        this.personSet1.add(new Person(53, "Joey", "Junior"));

        this.personSet2.add(new Person(12, "John", "Snow")); // Also in set 1.
        this.personSet2.add(new Person(25, "Zoey", "Rhodes"));
        this.personSet2.add(new Person(66, "Brian", "Adams"));
    }

    //
    // Public Methods
    //

    public void sorting() {
        // Print unsorted list.
        System.out.println(stronglyTypedList);

        // Using Comparable<Person>/compareTo(Person o)
        // Collections.sort(stronglyTypedList);

        // Using comparator.
        Comparator<Person> ageComparator = new Comparator<Person>() {
            public int compare(Person p1, Person p2) {
                return p1.getAge() - p2.getAge();
            }
        };
        // stronglyTypedList.sort(ageComparator);

        //Collections.sort(stronglyTypedList, ageComparator);

        // Using Lambda expression.
        //stronglyTypedList.sort(Comparator.comparing((Person p) -> p.getAge()));

        // Print sorted list.
        System.out.println(stronglyTypedList);
    }

    public void weaklyTypeList1() {
        System.out.println("*** Weekly Typed Lists ***");
        var list = weaklyTypedList.stream()
                .map(p -> ((Person)p).getAge())
                .filter( x -> (Integer)x > 30)
                .collect(Collectors.toList());
        System.out.println(list);
    }

    public void stronglyTypeList1() {
        System.out.println("*** Strongly Typed Lists ***");
//        System.out.println(stronglyTypedList);

        // Iteration: Foreach
//        for (Person p : stronglyTypedList ) {
//            p.setAge(0); // Note: cannot mutate primative types here, but this is a reference type so we can mutate.
//            System.out.println(p);
//        }

        // Iteration: Iterator
//        var itertorPerson = stronglyTypedList.iterator();
//        while (itertorPerson.hasNext()) {
//            var person = itertorPerson.next();
//            System.out.printf("Using iterator: %s\n", person);
//        }


        // Fixed size vs non-fixed size.
//        var personList = Arrays.asList( // Returns a fixed-size list backed by the specified array..  Cannot mutate.
//            new Person(10,"joe", "smith"),
//            new Person(11, "Mary", "Jane")
//        );
//        personList.add(new Person(12, "fred", "williams")); // Fails: UnsupportedOperationException
//
//        var personList2 = new ArrayList<>(Arrays.asList(
//                new Person(10,"joe", "smith"),
//                new Person(11, "Mary", "Jane")
//        ));
//        personList2.add(new Person(12, "fred", "williams")); // OK
//
//        System.out.println(personList);
    }

    public void queue1() {
        System.out.println("*** Queue ***");
        System.out.println(personQueue);

        var p = personQueue.remove();
        System.out.println(p);

//        System.out.println("*** Priority Queue ***");
//        System.out.println(personPriorityQueue);
//        personPriorityQueue.remove(); // Remove() forces the items in the queue to get re-prioritized (oldest first).
//        System.out.println(personPriorityQueue);
//        personPriorityQueue.remove();
//        System.out.println(personPriorityQueue);
    }

    public void stack1() {
        System.out.println("*** Stacks ***");
        System.out.println(personStack);

        var person = personStack.peek();  // Read the top of the stack.
        System.out.println(person);

        var searchPerson = new Person(12, "John", "Smith");
        if (personStack.contains(searchPerson)){
            System.out.println("YES: John Smith is in the queue.");
        }

        personStack.pop();
        System.out.println(personStack);
        personStack.pop();
        System.out.println(personStack);
        personStack.pop();
        System.out.println(personStack);
    }

    public void map1() {
        System.out.println("*** Maps ***");

        // Hash Map.
        System.out.println(personMap);
        var p1 = personMap.get("222-22-2222"); // Look up our person using the key.
        System.out.println(p1);
        var hashMapKeys = personMap.keySet().stream().toList(); // Get all keys from the HashMap.
        System.out.println(hashMapKeys);

        System.out.println("");

        // Dictionary.
        System.out.println(personDictionary);
        var p2 = personDictionary.get("222-22-2222"); // Look up our person using the key.
        System.out.println(p2);
        var dictionaryKeys = personDictionary.keys(); // Get all keys from the dictionary.
        List<String> list = Collections.list(dictionaryKeys);
        System.out.println(list);
    }

    public void sets1() {
        System.out.println(personSet1);
        System.out.println(personSet2);
        personSet1.addAll(personSet2); // Union: All the unique people from both sets.
        personSet1.retainAll(personSet2); // Intersection: The common people in both sets.
    }

    public void streams() {
        List<String> strList = Arrays.asList("alice", "bob", "charlie", "dave", "erik");

        // Approach 1: Unchained.
        Stream<String> myStream = strList.stream(); // Optionally use parallelStream() to use mutliple cores if available.
        var strStream = myStream.filter(x -> x.equals("alice"));
        var newStrList =  strStream.collect(Collectors.toList());
        System.out.println(newStrList);

        // Approach 2: Chained.
        //  System.out.println(strList.stream().filter(x -> x.equals("alice")).collect(Collectors.toList()));

        // Filter
//        var filterList = stronglyTypedList.stream()
//                .filter(p -> p.getAge() > 24)
//                .collect(Collectors.toList());
//        System.out.println(filterList);

       //  Map & Filter
//        var mapFilterList = stronglyTypedList.stream()
//                .map(p -> p.getAge()) // Can be replaced by method reference: Person::getAge  https://www.baeldung.com/java-method-references
//                .filter(x -> (Integer)x > 30)
//                .collect(Collectors.toList());
//        System.out.println(mapFilterList);

        // Increase Age
//        var newAgeList = stronglyTypedList.stream()
//                .map(p -> {
//                    int newAge = p.getAge();
//                    p.setAge(++newAge);
//                    return p;
//                })
//                .collect(Collectors.toList());
//        System.out.println(newAgeList);

    }
}
