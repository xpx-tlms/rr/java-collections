# Java Collections / Data Structures
A simple helper project that shows uses a Person and Cat class can be used in the following data structures:
- Weakly Typed List (Raw)
- Strongly Typed List (Parameterized)
- Queues
- Stacks
- Sets
- Map/Filter

# Weakly Typed List (Raw)
Raw types refer to using a generic type without specifying a type parameter.
For example, List is a raw type, while List<String> is a parameterized type.
- Usually require casts
- Not type safe, and some important kinds of errors will only appear at runtime
- Less expressive, and don't self-document in the same way as parameterized types

# Strongly Typed List (Parameterized)
Preferred method for creating a list of items.

# Queues
Queues: 
- Behave much like a line to a bathroom

Priority Queues:
- The order of the items in the queue change after a `remove()` operation based on the implementation of the `compareTo()` method in the classs

# Stacks
Behave like a stack of dishes.

# HashMap (Dictionary)
Uses a key/value system for fast lookups.  A key is associated with a value. 

# Sets
- Set is a data structure that is used as a collection of objects.
- Does not allow duplicates only if `hashCode()` has been implemented in a class
- Conceptually viewed as [venn diagrams](https://en.wikipedia.org/wiki/Venn_diagram):
  - `addAll()`: Union
  - `retainAll()`: Intersection

# Lambda/Method References
https://www.baeldung.com/java-method-references